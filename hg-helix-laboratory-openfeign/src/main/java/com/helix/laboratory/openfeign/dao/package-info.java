/**
* DAO  - 数据访问对象层，这里定义了 Mybatis Mapper 类。这里类建议使用 Mybatis Plus
* @author Allen Wang
* @date 2022/7/27 17:05
*/

package com.helix.laboratory.openfeign.dao;