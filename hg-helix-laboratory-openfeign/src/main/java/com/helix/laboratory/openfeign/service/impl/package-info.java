/**
* Service 实现层，请在这里定义具体业务逻辑，并实现 Service 接口层定义的接口；
* @author Allen Wang
* @date 2022/7/27 17:04
*/

package com.helix.laboratory.openfeign.service.impl;