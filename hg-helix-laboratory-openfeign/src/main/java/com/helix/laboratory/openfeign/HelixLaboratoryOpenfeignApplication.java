package com.helix.laboratory.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * All rights Reserved, Designed By www.hgplan.cn
 *
 * @author [Allen Wang]
 * @version V1.0
 * Copyright 2022 www.hgplan.cn Inc. All rights reserved.
 * @date 2022/8/3 17:40
 **/
@SpringBootApplication
@EnableFeignClients(basePackages = {"com"})
public class HelixLaboratoryOpenfeignApplication {

  public static void main(String[] args) {
      SpringApplication.run(HelixLaboratoryOpenfeignApplication.class);
  }
}