package com.hg.helix.laboratory.demo.entity;

import com.hg.helix.laboratory.demo.core.BaseEntity;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @description: TODO
 * @author: ZSF
 * @create: 2023/3/21 14:30
 **/
@Data
public class HgDemoUserInfoEntity extends BaseEntity {

    /** 用户id */
    private Long id;
    /** 用户名*/
    @NotBlank(message = "用户名不能为空")
    private String userName;
    /** 用户年龄 */
    private Integer age;
    /** 盐值 */
    private String salt;
    /** 用户收入 */
    private Integer salary;
    /** 电话号码 */
    @Size(max = 11, message = "手机号码长度不能超过11个字符")
    private Integer phone;
    /** 电子邮箱 */
    @Email(message = "邮箱格式不正确")
    private String email;
    /** 性别:0-女，1-男 */
    private String gender;
    /** 头像 */
    private String avatar;

}
