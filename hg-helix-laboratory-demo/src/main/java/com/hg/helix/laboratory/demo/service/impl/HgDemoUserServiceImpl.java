package com.hg.helix.laboratory.demo.service.impl;

import com.hg.helix.laboratory.demo.dao.HgDemoUserMapper;
import com.hg.helix.laboratory.demo.entity.HgDemoUserInfoEntity;
import com.hg.helix.laboratory.demo.service.HgDemoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: TODO
 * @author: ZSF
 * @create: 2023/3/21 15:05
 **/
@Service
public class HgDemoUserServiceImpl implements HgDemoUserService {

    @Autowired
    private HgDemoUserMapper hgDemoUserMapper;

    @Override
    public List<HgDemoUserInfoEntity> selectHgDemoUserInfoList(HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserMapper.selectHgDemoUserInfoList(userInfoEntity);
    }

    @Override
    public HgDemoUserInfoEntity selectHgDemoUserInfoById(Long id) {
        return hgDemoUserMapper.selectHgDemoUserInfoById(id);
    }

    @Override
    public int insertHgDemoUserInfo(HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserMapper.insertHgDemoUserInfo(userInfoEntity);
    }

    @Override
    public int updateHgDemoUserInfo(HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserMapper.updateHgDemoUserInfo(userInfoEntity);
    }

    @Override
    public int deleteHgDemoUserInfoById(Long id) {
        return hgDemoUserMapper.deleteHgDemoUserInfoById(id);
    }
}
