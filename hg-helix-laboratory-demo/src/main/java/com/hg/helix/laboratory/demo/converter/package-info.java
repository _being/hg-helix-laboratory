/**
* 数据转换层，如果需要对 VO和Entity 对象进行转换时候请使用  mapstruct 类库进行。
* @author Allen Wang
* @date 2022/7/27 17:06
*/

package com.hg.helix.laboratory.demo.converter;