package com.hg.helix.laboratory.demo.dao;

import com.hg.helix.laboratory.demo.entity.HgDemoUserInfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HgDemoUserMapper {

    /**
     * @description: 查询用户数据列表
     * @param userInfoEntity
     * @return:
     * @author: ZSF
     * @create: 2023/3/21 15:01
     ** */
    public List<HgDemoUserInfoEntity> selectHgDemoUserInfoList(HgDemoUserInfoEntity userInfoEntity);

    /**
     * @description: 根据ID查询用户数据
     * @param id
     * @return: com.hg.helix.laboratory.demo.entity.HgDemoUserInfoEntity
     * @author: ZSF
     * @create: 2023/3/21 15:02
     **/
    public HgDemoUserInfoEntity selectHgDemoUserInfoById(@Param("id") Long id);

    /**
     * @description: 添加用户信息
     * @param userInfoEntity
     * @return: int
     * @author: ZSF
     * @create: 2023/3/21 15:04
     **/
    public int insertHgDemoUserInfo(HgDemoUserInfoEntity userInfoEntity);

    /**
     * @description: 修改用户信息
     * @param userInfoEntity
     * @return: int
     * @author: ZSF
     * @create: 2023/3/21 15:04
     **/
    public int updateHgDemoUserInfo(HgDemoUserInfoEntity userInfoEntity);

    /**
     * @description: 删除用户数据（物理删除）
     * @param id
     * @return: int
     * @author: ZSF
     * @create: 2023/3/21 15:04
     **/
    public int deleteHgDemoUserInfoById(@Param("id") Long id);
}
