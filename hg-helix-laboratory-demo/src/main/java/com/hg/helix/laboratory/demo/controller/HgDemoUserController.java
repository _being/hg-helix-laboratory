package com.hg.helix.laboratory.demo.controller;

import com.hg.helix.laboratory.demo.dao.HgDemoUserMapper;
import com.hg.helix.laboratory.demo.entity.HgDemoUserInfoEntity;
import com.hg.helix.laboratory.demo.service.HgDemoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: 用户信息Controller
 * @author: ZSF
 * @create: 2023/3/21 15:07
 **/
@RestController
@RequestMapping("/hgDemoUser")
public class HgDemoUserController {

    @Autowired
    private HgDemoUserService hgDemoUserService;

    public List<HgDemoUserInfoEntity> selectHgDemoUserInfoList(HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserService.selectHgDemoUserInfoList(userInfoEntity);
    }

    public HgDemoUserInfoEntity selectHgDemoUserInfoById(Long id) {
        return hgDemoUserService.selectHgDemoUserInfoById(id);
    }

    public int insertHgDemoUserInfo(@Validated @RequestBody HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserService.insertHgDemoUserInfo(userInfoEntity);
    }

    public int updateHgDemoUserInfo(@Validated @RequestBody HgDemoUserInfoEntity userInfoEntity) {
        return hgDemoUserService.updateHgDemoUserInfo(userInfoEntity);
    }

    public int deleteHgDemoUserInfoById(Long id) {
        return hgDemoUserService.deleteHgDemoUserInfoById(id);
    }
}
